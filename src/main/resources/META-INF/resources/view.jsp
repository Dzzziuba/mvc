<%@ include file="init.jsp" %>

<a href="employee_list.jsp">All employees</a>
<liferay-portlet:actionURL name="employeeDetails" var="employeeDetails"></liferay-portlet:actionURL>

<div class="container">
<c:forEach items="${employeeList}" var="employee">
		<div class="form-group">
            <form action="<%=employeeDetails.toString()%>">
			<label class="control-label col-sm-2">${employee.name}</label>
			<div class="col-sm-10">
				<aui:input name="empId" type="hidden" value="${employee.empId}"/>
				<aui:button type="button" value="details" cssClass="btn btn-default"/>
            </form>
			</div>
		</div>
</c:forEach>