package com.roma.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.roma.constants.EmployeePortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.*;

import fr.smile.service.model.Employee;
import fr.smile.service.service.EmployeeLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import java.io.IOException;
import java.util.List;

/**
 * @author liferay
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.social",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Eployee MVC",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + EmployeePortletKeys.EmployeePortlet,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class EmployeePortlet extends MVCPortlet {

	@Override
	public void doView(RenderRequest req, RenderResponse res) throws IOException, PortletException {
		req.setAttribute("employeeList", getEmployees());
		super.doView(req, res);
	}
	public void employeeDetails(ActionRequest req, ActionResponse res) throws PortalException {
	    req.setAttribute("employee", EmployeeLocalServiceUtil.getEmployee(ParamUtil.getInteger(req,"empId")));
        res.setRenderParameter("jspPage", "/META-INF/resources/details.jsp");
    }
	private List<Employee> getEmployees(){
		return EmployeeLocalServiceUtil.getEmployees(0, EmployeeLocalServiceUtil.getEmployeesCount());
	}
}